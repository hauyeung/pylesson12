'''
Created on Jun 11, 2014

@author: hauyeung
'''

from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.base import MIMEBase
from email.mime.audio import MIMEAudio
from email.mime.multipart import MIMEMultipart
import mimetypes,os, urllib.request

def emailer(address, text, lst):
    msg = MIMEMultipart()
    msg.preamble = text
    msg['To'] = address
    msg['From']= 'test@from.com'

    print(os.getcwd())
    if lst != None:        
        for l in lst:
            url = urllib.request.pathname2url(l)
            ctype, enc = mimetypes.guess_type(url)
            maintype, subtype = ctype.split('/')
            if maintype == 'text':
                fp = open(l)
                part = MIMEText(fp.read(), _subtype=subtype)
                fp.close()
            elif maintype =='image':
                fp = open(l,'rb')
                part = MIMEImage(fp.read(), _subtype=subtype)
                fp.close()
            elif maintype =='audio':
                fp = open(l,'rb')
                part = MIMEAudio(fp.read(), _subtype=subtype)
                fp.close()
            else:
                fp = open(l,'rb')
                part = MIMEBase(maintype, subtype)
                part.set_payload(fp.read())
                fp.close()
            msg.add_header('Content-Disposition','attachment',filename=l)
                
            msg.attach(part)

    return msg

print (emailer('test@example.com','Email message',['file1.txt','file2.txt','file3.txt'])) 